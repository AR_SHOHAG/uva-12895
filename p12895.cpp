#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
using namespace std;

long long int power(long long int n,long long int r)
{
    if (r==1) return n;

    long long int p,q;
    p=r/2;
    q=power(n,p);

    if(r%2==0)
        return q*q;
    else
        return q*q*n;
}

int main()
{
    long long int r,i,n,l,T,sum=0,temp,m;
    char arr[100];

    cin>>T;

    while(T--){
        cin>>arr;
        l=strlen(arr);
        sum=0;
        n=atoi(arr);
        m=n;
        while(n!=0){
            r=n%10;
            temp=power(r,l);
            sum=sum+temp;
            n=n/10;
        }

        if(m==sum)
            cout<<"Armstrong"<<endl;
        else
            cout<<"Not Armstrong"<<endl;
    }
    return 0;
}
